Modifications to add a postit button to ipython notebook.

First find your profile_default directory: 

See here (http://ipython.org/ipython-doc/stable/interactive/notebook.html#configuring-the-ipython-notebook)

On new ipython these bash commands may be useful:
    ipython locate
    ipython profile create

On old version of ipython you can do the following

..code 
    import IPython
    ip=IPython.get_ipython()
    ip.config.ProfileDir 

Then copy the files in this directory to:
[profile_default]/static/custom

Edit profile.py to choose a cloud storage service
For gdrive service, install the google-api-python-client python package
