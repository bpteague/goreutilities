#!/usr/bin/env python
'''
Created on Nov 27, 2013

@author: yonatanf
'''
import os, sys, re
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPost, NewPost, EditPost, DeletePost, GetPosts
import datetime

try:
    from wp_info import USR, PWD, URL
except ImportError:
    USR = None
    PWD = None
    URL = None

if USR is None or PWD is None or URL is None:
    msg = 'Wordpress site and user details are not available.\n'
    msg += 'Create a file named wp_info.py somewhere in your PYTHONPATH.\n'
    msg += 'The contents of this file should be:\n'
    msg += 'USR = [wp username]\n'
    msg += 'PWD = [wp password]\n'
    msg += 'URL = [wp url]\n'
    import warnings
    warnings.warn(msg)

def connect(url=URL, user=USR, pwd=PWD):
    wp = Client(url, user, pwd)
    return wp

def make_post(content, title, tags, categories, status, date=None):
    now = datetime.datetime.now()
    post = WordPressPost()
    ## set post properties 
    post.title   = title
    post.content = content
    post.post_status = status
    post.terms_names = {
          'post_tag': tags,
          'category': categories
    }
    post.date_modified = now

    if date is not None:
        date = datetime.datetime.strptime(date, "%Y/%m/%d")
        post.date = date
    return post

def push_post(post, post_id):
    wp  = connect()
    now = datetime.datetime.now()
    ## check if post exists
    if post_id is not None:
        from xmlrpclib import Fault
        try:
            old_post = wp.call(GetPost(post_id))
        except Fault:
            old_post = None
    else:
        old_post = None
    ## push post
    if old_post is None:
        post_id = wp.call(NewPost(post))
    else:
        wp.call(EditPost(post_id, post))
    return post_id

def post_from_md(path, push=True, update_md=True, preprocess=True, **kwargs):
    from markdown_tools import to_html, process_md_meta
    ## load markdown file
    with open(path, 'rb') as f:
        md_txt = f.read().decode('utf-8')
    ## convert to html
    html, md = to_html(md_txt, preprocess=preprocess, **kwargs)
    ## extract/set metadata
    meta = process_md_meta(md.Meta)
    if 'title' in meta:
        title = meta['title']
    else:
        file_name = os.path.basename(path)
        title = os.path.splitext(file_name)[0]
    categories = meta.get('categories', None)
    tags       = meta.get('tags', None)
    post_id    = meta.get('post_id', None)
    status     = meta.get('status', 'publish')
    date       = meta.get('date', None)
    ## post
    post = make_post(html, title, tags, categories, status, date)
    if push:
        post_id = push_post(post, post_id)
    ##update md file with additional metadata
    if update_md:
        to_write = ''
        params = ['title', 'status', 'post_id', 'date']
        for p in params:
            if p not in meta and locals()[p] is not None:
                to_write += p + ': ' + locals()[p] + os.linesep
        if 'post_id' in meta:
            md_txt = re.sub('(?<=post_id: ).*', post_id, md_txt)
        with open(path, 'wb') as f:
            f.write((to_write + md_txt).encode("UTF-8"))
    return post, post_id


if __name__ == '__main__':
    print 'Do nothing'
    #wp = connect()
    #post = wp.call(GetPost(82))
    #post.content += 'testing' * 100
    #print post.id
    #print post.content
    #post.date = date
    #wp.call(EditPost(post.id, post))
