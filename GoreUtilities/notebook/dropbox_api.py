'''
Created on Nov 27, 2013

@author: yonatanf
'''
import os, sys, re
import dropbox
from dropbox import client

try:
    from db_token import TOKEN
except ImportError:
    from db_token import APP_KEY, APP_SECRET
    TOKEN = None

if TOKEN is None:
    #import warnings
    ##authorize_url = 'https://www.dropbox.com/1/oauth2/authorize?response_type=code&client_id=i1pfhjqtsn88u1k'
    #msg = 'An authorization code needs to be set before the Dropbox API can be used.\n'
    #msg += "1. Go to: " + authorize_url + '\n'
    #msg += "2. Click \"Allow\" (you might have to log in first).\n"
    #msg += "3. Copy the authorization code.\n"
    #msg += "4. Create a file named db_token.py somewhere in your PYTHONPATH.\n" 
    #msg += "The contents of this file should be:\n" 
    #msg += "TOKEN = [token value]"
    #warnings.warn(msg)
    # Get your app key and secret from the Dropbox developer website
    flow = dropbox.client.DropboxOAuth2FlowNoRedirect(APP_KEY, APP_SECRET)

    # Have the user sign in and authorize this token
    authorize_url = flow.start()
    print '1. Go to: ' + authorize_url
    print '2. Click "Allow" (you might have to log in first)'
    print '3. Copy the authorization code.'
    code = raw_input("Enter the authorization code here: ").strip()

    # This will fail if the user enters an invalid authorization code
    access_token, user_id = flow.finish(code)
    print access_token
    client = dropbox.client.DropboxClient(access_token)
    print 'linked account: ', client.account_info()

def get_url(path, token=TOKEN):
    """
    Get the url of a Dropbox file.
    """
    pth_in_db = re.search('(?<=Dropbox/).*', os.path.abspath(path)).group(0)
    api_client = dropbox.client.DropboxClient(token)
    url = api_client.share(pth_in_db, short_url=False)['url']
    direct_url = url.replace('www.dropbox.com', 'dl.dropboxusercontent.com', 1)
    return direct_url


if __name__ == '__main__':
    print get_url('dropbox_api.py')
    print get_url('../graph.py')
    pass
